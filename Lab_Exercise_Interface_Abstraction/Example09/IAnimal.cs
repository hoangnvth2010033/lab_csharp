﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Example09
{
     public  interface IAnimal
    {
        string Name { get;  }
        double Weight { get; }
        int FoodEaten { get; }
        void Eat();
        void ProduceSound();
        string ToString();
    }
}
