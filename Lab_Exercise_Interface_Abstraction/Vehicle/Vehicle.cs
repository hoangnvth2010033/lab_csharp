﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vehicle
{
    public abstract class Vehicle
    {
         double fuelQuantity { get; set; }
         double fuelConsumtion { get; set; }
         double Distance { get; set; }
       
        public virtual double countDistance()
        {
            return fuelQuantity / fuelConsumtion;
        }
        public virtual  void Compare()
        {
           
        }

    }
}
