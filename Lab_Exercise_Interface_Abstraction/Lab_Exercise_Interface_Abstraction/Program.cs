﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_Exercise_Interface_Abstraction
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Enter radius : ");
            var radius = int.Parse(Console.ReadLine());
            IDrawShape circle = new Circle(radius);
            circle.Draw();

            double width, height;
            do
            {
                Console.Write("\nEnter width : ");
                width = double.Parse(Console.ReadLine());
                Console.Write("Enter length : ");
                height = double.Parse(Console.ReadLine());

                if (width > height)
                {
                    Console.WriteLine("\n Width must be smaller than Length !");
                }
            } while (width > height);
            IDrawShape rect = new Rectangle(width, height);
            rect.Draw();

        }
    }
}
