﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ferrari
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Enter Driver name :");
            string driverName = Console.ReadLine();
            IFerrari ferrari = new Ferrari(driverName);

            Console.WriteLine("\n");
            Console.WriteLine(ferrari);
            Console.ReadLine();

        }
    }
}
