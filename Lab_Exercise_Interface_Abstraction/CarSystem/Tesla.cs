﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarSystem
{
    class Tesla:ICar,IELectricCar
    {
        public string Model { get; set; }
        public string Name { get; set; }
        public int Battery { get; set; }
        public string Color { get; set; }
        public Tesla()
        {

        }
        public Tesla(string model, string name,int battery,string color)
        {
            this.Model = model;
            this.Name = name;
            this.Battery = battery;
            this.Color = color;
        }
        public string Start()
        {
            return "Engine Start !!";
        }
        public string Stop()
        {
            return "Brakeee !";
        }
        public  override string ToString()
        {
            return Color + " Tesla " + Model + " with " + Battery +" battery. \n" + Start() + " ! \n" + Stop();
        }
    }
}
