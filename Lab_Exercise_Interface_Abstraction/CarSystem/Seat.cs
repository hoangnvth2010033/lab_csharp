﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarSystem
{
    public class Seat:ICar
    {
        public string Model { get; set; }
         public string Name { get; set; }
        public string Color { get; set; }
        public Seat()
        {

        }
        public Seat(string model, string name,string color)
        {
            this.Model = model;
            this.Name = name;
            this.Color = color;
        }
        public string Start()
        {
            return "Engine Start !!";
        }
        public string Stop()
        {
            return "Brakeee !";
        }

        public override string ToString()
        {
            return Color + " Seat " + Model + "\n" + Start() + " ! \n" + Stop();
        }
    }
}
