﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarSystem
{
    class Program
    {
        static void Main(string[] args)
        {
            Tesla tesla = new Tesla("Model 3","Tesla",2,"red");
            Console.WriteLine(tesla);

            Console.WriteLine("\n\n");
            Seat seat = new Seat("Leon", "Seat", "grey");
            Console.WriteLine(seat);
        }
    }
}
