﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_2_Menu
{
    class Schedule
    {
        public int STT { get; set; }
       public string TeamName1 { get; set; }
        public string TeamName2 { get; set; }
        public int MatchDay { get; set; }
        public int MatchMonth { get; set; }
        public int MatchYear { get; set; }
        public DateTime MatchTime { get; set; }
        public string MatchHour { get; set; }
        public string Field { get; set; }
    }
}
