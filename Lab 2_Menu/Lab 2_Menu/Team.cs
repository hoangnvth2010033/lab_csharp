﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_2_Menu
{
    class Team
    {
        public int STT { get; set; }
        public string TeamID { get; set; }
        public string TeamName { get; set; }
        public string Coach { get; set; }
    }
}
