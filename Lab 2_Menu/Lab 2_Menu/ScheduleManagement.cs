﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_2_Menu
{
    class ScheduleManagement
    {
        private List<Team> ListTeam = null;
        private List<Schedule> ListSchedule = null;
        public ScheduleManagement()
        {
            ListTeam = new List<Team>();
            ListSchedule = new List<Schedule>();
        }

        // Ham tao STT tang dan cho lich thi dau
        private int GenerateID()
        {
            int max = 1;
            if (ListSchedule != null && ListSchedule.Count > 0)
            {
                max = ListSchedule[0].STT;
                foreach (Schedule sc in ListSchedule)
                {
                    if (max < sc.STT)
                    {
                        max = sc.STT;
                    }
                }
                max++;
            }
            return max;
        }

        //Ham them moi lich thi dau ( TH ket noi dc vs menu khac)
        public void CreateSchedule(string name1 , string name2)
        {
            TeamManagement teamManagement = new TeamManagement();           
            Schedule schedule = new Schedule();
            Team team = teamManagement.FindByOnlyName(name1);
            Team team1 = teamManagement.FindByOnlyName(name2);
            schedule.STT = GenerateID();

            if (team != null && team1 != null)
            {
                Console.Write("Nhập ngày thi đấu :");
                 schedule.MatchDay = Convert.ToInt32(Console.ReadLine());
                

                Console.Write("Nhập giờ thi đấu :");
                schedule.MatchHour = Console.ReadLine();

                Console.Write("Nhập sân thi đấu :");
                schedule.Field = Console.ReadLine();
                ListSchedule.Add(schedule);
                Console.WriteLine("\n\t\tCập nhật lịch thi đấu thành công !\n");
            }
            else
            {
                Console.WriteLine("\nĐội bóng có tên = {0} hoặc {1} không tồn tại !", name1,name2);
            }
        }

        //Ham boc lich
        public int CountSchedule()
        {
            int count = 0;
            if(ListSchedule != null)
            {
                count = ListSchedule.Count;
            }
            return count;
        }

        //Ham them moi lich ko can ket noi
        public void AddSchedule()
        {
            Schedule schedule = new Schedule();
            schedule.STT = GenerateID();

            Console.Write("Nhập tên đội bóng 1 :");
            schedule.TeamName1 = Convert.ToString(Console.ReadLine());

            Console.Write("Nhập tên đội bóng 2 :");
            schedule.TeamName2 = Convert.ToString(Console.ReadLine());

            Console.WriteLine("Nhập ngày thi đấu ");
            Console.Write("Ngày :");
            schedule.MatchDay = Convert.ToInt32(Console.ReadLine());
            Console.Write("Tháng :");
            schedule.MatchMonth = Convert.ToInt32(Console.ReadLine());
            Console.Write("Năm :");
            schedule.MatchYear = Convert.ToInt32(Console.ReadLine());

            DateTime matchTime = new DateTime(schedule.MatchYear, schedule.MatchMonth, schedule.MatchDay);
            schedule.MatchTime = matchTime;

            Console.Write("Nhập giờ thi đấu cụ thể :");
            schedule.MatchHour = Console.ReadLine();

            Console.Write("Nhập sân thi đấu :");
            schedule.Field = Console.ReadLine();

            ListSchedule.Add(schedule);
        }

        //Ham cap nhat lich thi dau theo STT
        public void updateSchedule(int STT)
        {
            Schedule schedule = FindBySTT(STT);
            if(schedule != null)
            {
                Console.Write("Nhập tên đội bóng 1 :");
                string name1 = Convert.ToString(Console.ReadLine());
                //neu ko nhap j thi ten ko doi
                if (name1 != null && name1.Length > 0)
                {
                    schedule.TeamName1 = name1;
                }

                Console.Write("Nhập tên đội bóng 2 :");
                string name2 = Convert.ToString(Console.ReadLine());
                if (name2 != null && name2.Length > 0)
                {
                    schedule.TeamName2 = name2;
                }

                Console.WriteLine("\tNhập ngày thi đấu ");
                Console.Write("Ngày :");
                string day = Convert.ToString(Console.ReadLine());  
                if(day!=null && day.Length > 0)
                {
                    schedule.MatchDay = Convert.ToInt32(day);

                }
                
                Console.Write("Tháng :");
                string month = Convert.ToString(Console.ReadLine());
                if (month != null && month.Length > 0)
                {
                    schedule.MatchMonth = Convert.ToInt32(month);
                }

                Console.Write("Năm :");
                string year = Convert.ToString(Console.ReadLine());
                if (year != null && year.Length > 0)
                {
                    schedule.MatchYear = Convert.ToInt32(year);

                }

                DateTime matchTime = new DateTime(schedule.MatchYear, schedule.MatchMonth, schedule.MatchDay);
                schedule.MatchTime = matchTime;

                Console.Write("Nhập giờ thi đấu cụ thể :");
                string hour = Convert.ToString(Console.ReadLine());
                //neu ko nhap j thi ten ko doi
                if (hour != null && hour.Length > 0)
                {
                    schedule.MatchHour = hour;
                }

                Console.Write("Nhập sân thi đấu :");
                string field = Convert.ToString(Console.ReadLine());
               
                if (field != null && field.Length > 0)
                {
                    schedule.Field = field;
                }
            }
            else
            {
                Console.WriteLine("\nLịch thi đấu có STT = {0} không tồn tại !", STT);
            }
        }

        //Ham tim kiem theo STT
        public Schedule FindBySTT(int STT)
        {
            Schedule searchSTT = null;
            if(ListSchedule != null && ListSchedule.Count > 0)
            {
                foreach(Schedule schedule in ListSchedule)
                {
                    if(schedule.STT == STT)
                    {
                        searchSTT = schedule;
                    }
                }
            }
            return searchSTT;
        }

        //Ham xoa lich thi dau theo STT
        public bool DeletedBySTT(int STT)
        {
            bool IsDeleted = false;
            Schedule schedule = FindBySTT(STT);
            if (schedule != null)
            {
                IsDeleted = ListSchedule.Remove(schedule);
            }
            return IsDeleted;
        }

        //Ham hien thi danh sach lich thi dau
        public void ShowSchedule(List<Schedule> schedules)
        {
            Console.WriteLine("{0, -5} | {1, -20} | {2, -20} | {3, -25} | {4, -12} | {5,-18} ",
                "STT", "Team 1", "Team 2", "Ngày thi đấu" , "Giờ cụ thể" , "Sân thi đấu");
            if (schedules != null && schedules.Count > 0)
            {
                foreach(Schedule schedule in schedules)
                {
                    Console.WriteLine("{0, -5} | {1, -20} | {2, -20} | {3, -25} | {4, -12} | {5,-18} ",
               schedule.STT, schedule.TeamName1, schedule.TeamName2, schedule.MatchTime, schedule.MatchHour, schedule.Field);

                }
            }
            Console.WriteLine();
        }

        //Ham tra ve danh sach lich thi dau hien tai
        public List<Schedule> GetSchedule()
        {
            return ListSchedule;
        }

    }
}
