﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_2_Menu
{
    class Result
    {
        public int STT { get; set; }
        public string TeamID1 { get; set; }
        public string TeamID2 { get; set; }
        public string TeamName1 { get; set; }
        public string TeamName2 { get; set; }
        public int score1 { get; set; }
        public int score2 { get; set; }
        public int point1 { get; set; }
        public int point2 { get; set; }
       
        public int point { get; set; }

    }
}
