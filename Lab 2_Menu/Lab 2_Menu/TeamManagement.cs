﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;  

namespace Lab_2_Menu
{
    class TeamManagement
    {
        private List<Team> listTeam = null;
        public TeamManagement()
        {
            listTeam = new List<Team>(); 
        }

        // Ham tao STT tang dan cho doi bong
        private int GenerateID()
        {
            int max = 1;
            if( listTeam != null && listTeam.Count >0)
            {
                max = listTeam[0].STT;
                foreach (Team t in listTeam)
                {
                    if (max < t.STT)
                    {
                        max = t.STT;
                    }
                }
                max++;
            }
            return max;
        }

        //Ham dem so doi bong
        public int countTeam()
        {
            int count = 0;
            if(listTeam != null)
            {
                count = listTeam.Count;
            }
            return count;
        }

        //Ham them moi doi bong
        public void createTeam()
        {
            Team team = new Team();
            team.STT = GenerateID();
            Console.Write("Nhập mã đội :");
            team.TeamID = Console.ReadLine();

            Console.Write("Nhập tên đội :");
            team.TeamName = Console.ReadLine();

            Console.Write("Nhập tên Huấn luyện viên :");
            team.Coach = Console.ReadLine();

            listTeam.Add(team);
        }

        //Ham cap nhat doi bong theo ID
        public void updateTeam(string ID)
        {
            Team team = FindByID(ID);
            if(team != null)
            {
                Console.Write("Nhập tên đội bóng :");
                string name = Convert.ToString(Console.ReadLine());
                //neu ko nhap j thi ten ko doi
                if(name!=null && name.Length > 0)
                {
                    team.TeamName = name;
                }

                Console.Write("Nhập tên HLV :");
                string coach = Convert.ToString(Console.ReadLine());
                if (coach != null && coach.Length > 0)
                {
                    team.Coach = coach;
                }
                Console.WriteLine("\n\t\tCập nhật đội bóng thành công !\n");
            }
            else
            {
                Console.WriteLine("\nĐội bóng có ID = {0} không tồn tại !", ID);
            }
        }

        // Ham tim kiem theo ID
        public Team FindByID(string ID)
        {
            Team searchID = null;
            if(listTeam != null && listTeam.Count > 0)
            {
                foreach(Team team in listTeam)
                {
                   if(team.TeamID == ID)
                    {
                        searchID = team;
                    }
                }
            }
            return searchID;
        }


        // Ham tim kiem theo ten doi; tra ve 1 doi
        public Team FindByOnlyName(string Name)
        {
            Team searchName = null;
            if (listTeam != null && listTeam.Count > 0)
            {
                foreach (Team team in listTeam)
                {
                    if (team.TeamName == Name)
                    {
                        searchName = team;
                    }
                }
            }
            return searchName;
        }

        //Ham tim kiem doi bong theo ten; tra ve 1 danh sach doi bong
        public List<Team> FindByName( string name)
        {
            List<Team> searchID = new List<Team>();
            if(listTeam != null && listTeam.Count > 0)
            {
                foreach(Team team in listTeam)
                {
                    if (team.TeamName.ToUpper().Contains(name.ToUpper()))
                    {
                        searchID.Add(team);
                    }
                }
            }
            return searchID;
        }

        // Ham xoa doi bong theo ID
        public bool DeleteByID(string ID)
        {
            bool IsDeleted = false;
            Team team = FindByID(ID);
            if(team != null)
            {
                IsDeleted = listTeam.Remove(team);
            }
            return IsDeleted;
        }

        //Ham sap xep doi bong theo ID tang dan
        public void SortByID()
        {
            listTeam.Sort(delegate (Team team1, Team team2)
            {
                return team1.TeamID.CompareTo(team2.TeamID);
            });
        }

        //Ham sap xep doi bong theo ten tang dan
        public void SortByName()
        {
            listTeam.Sort(delegate (Team team1, Team team2)
            {
                return team1.TeamName.CompareTo(team2.TeamName);
            });
        }

        //Ham hien thi danh sach doi bong
        public void ShowTeam(List<Team> listTeam)
        {
            Console.WriteLine("{0, -20} | {1, -20} | {2, -20} | {3, -20}",
                "STT", "Mã Đội", "Tên Đội", "Tên HLV");
            if(listTeam != null && listTeam.Count > 0)
            {
                foreach(Team team in listTeam)
                {
                    Console.WriteLine("{0, -20} | {1, -20} | {2, -20} | {3, -20}",
                        team.STT, team.TeamID, team.TeamName, team.Coach);
                }
            }
            Console.WriteLine();
        }

        //Ham tra ve danh sach doi bong hien tai
        public List<Team> GetTeams()
        {
            return listTeam;
        }

    }
}
