﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_2_Menu
{
    class ResultManagement
    {
        private List<Team> ListTeam = null;
        private List<Schedule> ListSchedule = null;
        private List<Result> ListResult = null;

        public ResultManagement()
        {
            ListTeam = new List<Team>();
            ListSchedule = new List<Schedule>();
            ListResult = new List<Result>();
        }

        //Ham tao STT tang dan cho Ket qua thi dau
        private int GenerateID()
        {
            int max = 1;
            if(ListResult != null && ListResult.Count > 0)
            {
                max = ListResult[0].STT;
                foreach(Result rs in ListResult)
                {
                    if (max < rs.STT)
                    {
                        max = rs.STT;
                    }
                }
                max++;
            }
            return max;
        }

      

        //Ham dem ket qua
        public int CountResult()
        {
            int count = 0;
            if(ListResult != null)
            {
                count = ListResult.Count;
            }
            return count;
        }

        //Ham them ket qua tran dau khong can ket noi 
        public void AddResult()
        {

            Result result = new Result();
            result.STT = GenerateID();

            Console.Write("\n\t\tTrận đấu giữa hai đội :");
            Console.Write("\nDoi 1 :\nNhập tên : ");
            result.TeamName1 = Convert.ToString(Console.ReadLine());
            Console.Write("Nhập mã đội ' {0} ' : ", result.TeamName1);
            result.TeamID1 = Convert.ToString(Console.ReadLine());
            
            Console.Write("\nDoi 2 :\nNhập tên : ");
            result.TeamName2 = Convert.ToString(Console.ReadLine());
            Console.Write("Nhập mã đội ' {0} ' : ", result.TeamName2);
            result.TeamID2 = Convert.ToString(Console.ReadLine());

            Console.WriteLine("\tNhập tỉ số  : ");
            Console.Write("\t  {0} : ",result.TeamID1);
            result.score1 = Convert.ToInt32(Console.ReadLine());
            Console.Write("\t {0} : ",result.TeamID2);
            result.score2 = Convert.ToInt32(Console.ReadLine());


            ListResult.Add(result);
        }

        //Ham cap nhat ket qua tran dau 
        public void UpdateResult(int STT)
        {
            Result result = FindBySTT(STT);
            if (result != null)
            {
                Console.Write("\n Nhập tên đội 1 :");
                string name1 = Convert.ToString(Console.ReadLine());
                if (name1 != null && name1.Length > 0) {
                    result.TeamName1 = name1;
                }
                Console.Write("\n Nhập mã đội 1 :");
                string ID1 = Convert.ToString(Console.ReadLine());
                if (ID1 != null && ID1.Length > 0)
                {
                    result.TeamID1 = ID1;
                }

                Console.Write("\n Nhập tên đội 2 :");
                string name2 = Convert.ToString(Console.ReadLine());
                if (name2 != null && name2.Length > 0)
                {
                    result.TeamName2 = name2;
                }
                Console.Write("\n Nhập mã đội 2 :");
                string ID2 = Convert.ToString(Console.ReadLine());
                if (ID2 != null && ID2.Length > 0)
                {
                    result.TeamID2 = ID2;
                }

                Console.WriteLine("\tNhập tỉ số  : ");
                Console.Write("\t  {0} : ", result.TeamID1);
                string score1 = Convert.ToString(Console.ReadLine());
                if(score1 != null && score1.Length > 0)
                {
                    result.score1 = Convert.ToInt32(score1);
                }

                Console.Write("\t  {0} : ", result.TeamID2);
                string score2 = Convert.ToString(Console.ReadLine());
                if (score2 != null && score2.Length > 0)
                {
                    result.score2 = Convert.ToInt32(score2);
                }
            }
            else
            {
                Console.WriteLine("\nTran đấu có STT = {0} không tồn tại !", STT);
            }
        }



        //Ham tim kiem theo STT
        public Result FindBySTT(int STT)
        {
            Result searchSTT = null;
            if(ListResult != null && ListResult.Count > 0)
            {
                foreach(Result result in ListResult)
                {
                    if (result.STT == STT)
                    {
                        searchSTT = result;
                    }
                }
            }
            return searchSTT;
        }

        //Ham xoa ket qua theo STT
        public bool DeletedBySTT(int STT)
        {
            bool IsDeleted = false;
            Result result = FindBySTT(STT);
            if (result != null)
            {
                IsDeleted = ListResult.Remove(result);
            }
            return IsDeleted;
        }

        //Ham hien thi ket qua 
        public void ShowResult(List<Result> results)
        {
            Console.WriteLine("{0, -5} | {1,-15} | {2,-15} | {3,-15}",
                "STT", "Team 1", "Team 2", "Tỉ số");
            if(results != null && results.Count > 0)
            {
                foreach(Result result in results)
                {
                    Console.WriteLine("{0, -5} | {1,-15} | {2,-15} | {3} - {4} ",
                        result.STT, result.TeamID1, result.TeamID2, result.score1, result.score2);
                }
            }
            Console.WriteLine();
        }

        //Ham tra ve danh sach ket qua
        public List<Result> GetResult()
        {
            return ListResult;
        }

    }
}
