﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_2_Menu
{
 

    class Program
    {
        struct Team
        {
            public string name;
            public string ID;
            public int win;
            public int draw;
            public int lose;
            public int point;
            public static void Main(string[] args)
        {
       
            Console.InputEncoding = Encoding.UTF8;
            Console.OutputEncoding = Encoding.UTF8;
            menuMain();
        }

        static void ResetMenu()
        {
            Console.WriteLine("\n\t\tNhấn phím bất kì để tiếp tục...");
            Console.ReadLine();
            Console.Clear();
        }
        static void menuMain()
        {
            int n,key ;
            Team[] team = new Team[1000];

            while (true)
            {
                Console.WriteLine("\n\n\t\t\t    HỆ THỐNG V-LEAGUE 2021 \n  ");
                Console.WriteLine("\t\t================== MAIN MENU ===================");
                Console.WriteLine("\t\t|     1. Quản lý danh sách đội bóng            |");
                Console.WriteLine("\t\t|     2. Quản lý lịch thi đấu                  |");
                Console.WriteLine("\t\t|     3. Quản lý kết quả thi đấu               |");
                Console.WriteLine("\t\t|     4. Thống kê thành tích                   |");
                Console.WriteLine("\t\t|     0. Thoát                                 |");
                Console.WriteLine("\t\t================================================");
                Console.Write("Nhập lựa chọn : ");
                key = Convert.ToInt32(Console.ReadLine());
                switch (key)
                {
                    case 1:
                        Console.Clear();
                        menuQuanLyDS();
                        Console.Clear();
                        break;
                    case 2:
                        Console.Clear();
                        menuSchedule();
                        Console.Clear();
                        break;
                    case 3:
                        Console.Clear();
                        menuResult();
                        Console.Clear();
                        break;
                    case 4:
                        Console.Clear();
                        do
                        {
                            Console.Write("\n\n\t\tNhập số đội bóng (0 < num < 100 ) :");
                            n = Convert.ToInt32(Console.ReadLine());
                                if(n < 0 || n > 100)
                                {
                                    Console.WriteLine("\t Số đội bóng không hợp lệ; Mời nhập lai !");
                                }

                        } while (n < 0 || n > 100);
                        NhapTeam( team ,n);
                        SapXep(team, n);
                        XuatTeam(team, n);
                        ResetMenu();
                        break;
                    case 0:
                        Console.WriteLine("\n\n\t\t \u263A \u263A \u263A Goodbye !! \u263A \u263A \u263A\n\n");
                        Environment.Exit(0);
                        break;
                    default:
                        Console.Clear();
                        Console.WriteLine("\n\n\t\t\tChức năng không hợp lệ | Mời nhập lại !\n\n");
                        break;
                }
            }
        }
      

       
        //Menu quản lý danh sách đội bóng
        static void menuQuanLyDS()
        {
            TeamManagement teamManagement = new TeamManagement();
            int key;
            string answer;
                
            while (true)
            {
                Console.WriteLine("\t\t\t Quản lý đội bóng");

                Console.WriteLine("\t\t============================================");
                Console.WriteLine("\t\t=    1. Xem danh sách đội bóng             =");
                Console.WriteLine("\t\t=    2. Cập nhật danh sách đội bóng        =");
                Console.WriteLine("\t\t=    3. Thêm mới một đội bóng              =");
                Console.WriteLine("\t\t=    4. Xóa một đội bóng                   =");
                Console.WriteLine("\t\t=    5. Xem danh sách theo thứ tự mã đội   =");
                Console.WriteLine("\t\t=    6. Xem danh sách theo tên đội         =");
                Console.WriteLine("\t\t=    0. Trở về Menu chính                  =");
                Console.WriteLine("\t\t============================================");
                Console.Write("Nhập lựa chọn : ");
                key = Convert.ToInt32(Console.ReadLine());
                switch (key)
                {
                    case 1:
                        if (teamManagement.countTeam() > 0)
                        {                    
                            Console.WriteLine("\n\t\t| Xem danh sách đội bóng |\n");                           
                            teamManagement.ShowTeam(teamManagement.GetTeams());
                        }
                        else
                        {
                            Console.Clear();
                            Console.WriteLine("\n\t\tDanh sách đội bóng trống !\n\n");
                        }
                        ResetMenu();
                        break;
                        
                    case 2:
                        if (teamManagement.countTeam() > 0)
                        {
                            string id;                          
                            Console.WriteLine("\n\t\t| Cập nhật thông tin đội bóng |\n");                    
                            Console.Write("\nNhập Team ID :");
                            id = Console.ReadLine();
                            teamManagement.updateTeam(id);                          
                        }
                        else
                        {                        
                            Console.WriteLine("\n\t\tDanh sách đội bóng trống !\n\n");
                        }
                        Console.Write("\t\tBạn có muốn tiếp tục không ? (Y/N) :");
                            answer = Convert.ToString(Console.ReadLine());
                            if (answer == "Y" || answer == "y")
                            {
                                Console.Clear();
                                goto case 2;
                            }
                            else if (answer == "N" || answer == "n")
                            {
                                Console.Clear();
                            }
                            else
                            {
                                Console.Clear();
                                Console.WriteLine("\n\t\tBạn đã nhập sai => Tự động thoát chương trình !\n\n");
                            }
                            break;
                    case 3:                      
                        Console.WriteLine("\n\t\t| Thêm mới đội bóng |\n");                       
                        teamManagement.createTeam();
                        Console.WriteLine("\n\t\tThêm đội bóng thành công !\n");
                        Console.Write("\t\tBạn có muốn tiếp tục không ? (Y/N) :");
                            answer = Convert.ToString(Console.ReadLine());
                            if (answer == "Y" || answer == "y")
                            {
                                Console.Clear();
                                goto case 3;
                            }
                            else if (answer == "N" || answer == "n")
                            {
                                Console.Clear();
                            }
                            else
                            {
                                Console.Clear();
                                Console.WriteLine("\n\t\tBạn đã nhập sai => Tự động thoát chương trình !\n\n");
                            }

                            break;
                    case 4:
                        if (teamManagement.countTeam() > 0)
                        {
                            string id;
                            Console.WriteLine("\n\t\t| Xóa đội bóng |\n");                          
                            Console.Write("\nNhập Team ID :");
                            id = Console.ReadLine();
                            if (teamManagement.DeleteByID(id))
                            {
                                Console.WriteLine("\nĐội bóng có id = {0} đã bị xóa ", id);
                            }
                            else
                            {
                                Console.WriteLine("\nĐội bóng có thể đã bị xóa hoặc không tồn tại !");
                            }
                        }
                        else
                        {
                            Console.Clear();
                            Console.WriteLine("\n\t\tDanh sách đội bóng trống !\n\n");
                            
                        }
                        Console.Write("\t\tBạn có muốn tiếp tục không ? (Y/N) :");
                            answer = Convert.ToString(Console.ReadLine());
                            if (answer == "Y" || answer == "y")
                            {
                                Console.Clear();
                                goto case 4;
                            }
                            else if (answer == "N" || answer == "n")
                            {
                                Console.Clear();
                            }
                            else
                            {
                                Console.Clear();
                                Console.WriteLine("\n\t\tBạn đã nhập sai => Tự động thoát chương trình !\n\n");
                            }
                            break;
                    case 5:
                        if (teamManagement.countTeam() > 0)
                        {                        
                            Console.WriteLine("\n\t\t| Xem danh sách theo thứ tự ID |\n");                        
                            teamManagement.SortByID();
                            teamManagement.ShowTeam(teamManagement.GetTeams());
                        }
                        else
                        {
                            Console.WriteLine("\n\t\tDanh sách đội bóng trống !");
                        }
                        ResetMenu();
                        break;
                    case 6:
                        if (teamManagement.countTeam() > 0)
                        {                           
                            Console.WriteLine("\n\t\t| Xem danh sách theo tên |\n");                          
                            teamManagement.SortByName();
                            teamManagement.ShowTeam(teamManagement.GetTeams());
                        }
                        else
                        {
                            Console.WriteLine("\n\t\tDanh sách đội bóng trống !");
                        }
                        ResetMenu();
                        break;

                    case 0:
                        Console.Clear();
                        menuMain();
                        break;
                    default:
                        Console.Clear();
                        Console.WriteLine("\n\n\t\t\tChức năng không hợp lệ | Mời nhập lại !\n\n");
                        break;
                }
            }
        }

       
       
        // Menu quản lý lịch thi đấu
        static void menuSchedule()
        {
            TeamManagement teamManagement = new TeamManagement();
            ScheduleManagement scheduleManagement = new ScheduleManagement();
            int key;
            string answer;
            while (true)
            {
                Console.WriteLine("\t\t\t Quản lý lịch thi đấu");
                Console.WriteLine("\t\t============================================");
                Console.WriteLine("\t\t=    1. Xem lịch thi đấu                   =");
                Console.WriteLine("\t\t=    2. Cập nhật lịch thi đấu              =");
                Console.WriteLine("\t\t=    3. Thêm mới lịch thi đấu              =");
                Console.WriteLine("\t\t=    4. Xóa lịch thi đấu                   =");
                Console.WriteLine("\t\t=    0. Trở về Menu chính                  =");
                Console.WriteLine("\t\t============================================");
                Console.Write("Nhập lựa chọn : ");
                key = Convert.ToInt32(Console.ReadLine());
                switch (key)
                {
                    case 1:
                        
                       if (scheduleManagement.CountSchedule() > 0)
                        {
                            Console.WriteLine("\n\t\t|Xem danh sách lịch thi đấu |\n");
                            scheduleManagement.ShowSchedule(scheduleManagement.GetSchedule());
                        }
                        else
                        {
                            Console.Clear();
                            Console.WriteLine("\n\t\tDanh sách lịch thi đấu trống !\n\n");
                        }
                        ResetMenu();                     
                        break;

                    case 2:
                        if (scheduleManagement.CountSchedule() > 0)
                        {
                            int STT;
                            Console.WriteLine("\n\t\t| Cập nhật lịch thi đấu |\n");
                            Console.Write("\nNhập STT lịch thi đấu :");
                            STT =Convert.ToInt32( Console.ReadLine());
                            scheduleManagement.updateSchedule(STT);
                        }
                        else
                        {
                            Console.WriteLine("\n\t\tDanh sách lịch thi đấu trống !\n\n");
                        }
                        Console.Write("\t\tBạn có muốn tiếp tục không ? (Y/N) :");
                        answer = Convert.ToString(Console.ReadLine());
                        if (answer == "Y" || answer == "y")
                        {
                            Console.Clear();
                            goto case 2;
                        }
                        else if (answer == "N" || answer == "n")
                        {
                            Console.Clear();
                        }
                        else
                        {
                            Console.Clear();
                            Console.WriteLine("\n\t\tBạn đã nhập sai => Tự động thoát chương trình !\n\n");
                        }
                        break;
                        
                    case 3:
                        /*   string name1,name2;
                           Console.WriteLine("\n3. Them moi doi bong ");
                           Console.Write("\nNhập Team name 1 :");
                           name1 = Console.ReadLine();
                           Console.Write("\nNhập Team name 2 :");
                           name2 = Console.ReadLine();
                           scheduleManagement.CreateSchedule(name1,name2); */

                        Console.WriteLine("\n\t\t| Thêm mới lịch thi đấu |\n");
                        scheduleManagement.AddSchedule();
                        Console.WriteLine("\n\t\tThêm lịch thi đấu thành công !\n");
                        Console.Write("\t\tBạn có muốn tiếp tục không ? (Y/N) :");
                        answer = Convert.ToString(Console.ReadLine());
                        if (answer == "Y" || answer == "y")
                        {
                            Console.Clear();
                            goto case 3;
                        }
                        else if (answer == "N" || answer == "n")
                        {
                            Console.Clear();
                        }
                        else
                        {
                            Console.Clear();
                            Console.WriteLine("\n\t\tBạn đã nhập sai => Tự động thoát chương trình !\n\n");
                        }
                        break;

                    case 4:
                        if (scheduleManagement.CountSchedule() > 0)
                        {
                           int STT;
                            Console.WriteLine("\n\t\t| Xóa lịch thi đấu |\n");
                            Console.Write("\nNhập STT lịch thi đấu :");
                            STT =Convert.ToInt32( Console.ReadLine());
                            if (scheduleManagement.DeletedBySTT(STT))
                            {
                                Console.WriteLine("\nTrận đấu có STT = {0} đã bị xóa ", STT);
                            }
                            else
                            {
                                Console.WriteLine("\nTrận đấu có thể đã bị xóa hoặc không tồn tại !");
                            }
                        }
                        else
                        {
                            Console.Clear();
                            Console.WriteLine("\n\t\tDanh sách lịch thi đấu trống !\n\n");
                        }
                        Console.Write("\t\tBạn có muốn tiếp tục không ? (Y/N) :");
                        answer = Convert.ToString(Console.ReadLine());
                            if (answer == "Y" || answer == "y")
                            {
                                Console.Clear();
                                goto case 4;
                            }
                            else if (answer == "N" || answer == "n")
                            {
                                Console.Clear();
                            } 
                        else
                        {
                            Console.Clear();
                            Console.WriteLine("\n\t\tBạn đã nhập sai => Tự động thoát chương trình !\n\n");
                        }
                        break;
                       
                    case 0:
                        Console.Clear();
                        menuMain();
                        break;
                    default:
                        Console.Clear();
                        Console.WriteLine("\n\n\t\t\tChức năng không hợp lệ | Mời nhập lại !\n\n");
                        break;

                }
            }
        }

        static void menuResult()
        {
            ResultManagement resultManagement = new ResultManagement();
           

            int key;
            string answer;
            while (true)
            {
                Console.WriteLine("\t\t\t Kết quả thi đấu");
                Console.WriteLine("\t\t=============================================");
                Console.WriteLine("\t\t=    1. Xem kết quả thi đấu                 =");
                Console.WriteLine("\t\t=    2. Cập nhật kết quả thi đấu            =");
                Console.WriteLine("\t\t=    3. Thêm mới kết quả thi đấu            =");
                Console.WriteLine("\t\t=    4. Xóa kết quả thi đấu                 =");
                Console.WriteLine("\t\t=    5. Thống kê thành tích                 =");
                Console.WriteLine("\t\t=    0. Trở về Menu chính                   =");
                Console.WriteLine("\t\t=============================================");
                Console.Write("Nhập lựa chọn : ");
                key = Convert.ToInt32(Console.ReadLine());
                switch (key)
                {
                    case 1:

                        if (resultManagement.CountResult() > 0)
                        {
                            Console.WriteLine("\n\t\t|Xem danh sách kết quả thi đấu |\n");
                            resultManagement.ShowResult(resultManagement.GetResult());
                        }
                        else
                        {
                            Console.Clear();
                            Console.WriteLine("\n\t\tDanh sách kết quả trống !\n\n");
                        }
                        ResetMenu();
                        break;
                    case 2:
                        if (resultManagement.CountResult() > 0)
                        {
                            int STT;
                            Console.WriteLine("\n\t\t| Cập nhật kết quả thi đấu |\n");
                            Console.Write("\nNhập STT kết quả trận đấu :");
                            STT = Convert.ToInt32(Console.ReadLine());
                            resultManagement.UpdateResult(STT);
                        }
                        else
                        {
                            Console.WriteLine("\n\t\tDanh sách kết quả trống !\n\n");
                        }
                        Console.Write("\t\tBạn có muốn tiếp tục không ? (Y/N) :");
                        answer = Convert.ToString(Console.ReadLine());
                            if (answer == "Y" || answer == "y")
                            {
                                Console.Clear();
                                goto case 2;
                            }
                            else if (answer == "N" || answer == "n")
                            {
                                Console.Clear();
                            }
                            else
                            {
                                Console.Clear();
                                Console.WriteLine("\n\t\tBạn đã nhập sai => Tự động thoát chương trình !\n\n");
                            }

                            break;
                    case 3:
                        Console.WriteLine("\n\t\t| Thêm mới kết quả |\n");
                        resultManagement.AddResult();
                        Console.WriteLine("\n\t\tThêm kết quả thành công !\n");
                        Console.Write("\t\tBạn có muốn tiếp tục không ? (Y/N) :");
                            answer = Convert.ToString(Console.ReadLine());
                            if (answer == "Y" || answer == "y")
                            {
                                Console.Clear();
                                goto case 3;
                            }
                            else if (answer == "N" || answer == "n")
                            {
                                Console.Clear();
                            }
                            else
                            {
                                Console.Clear();
                                Console.WriteLine("\n\t\tBạn đã nhập sai => Tự động thoát chương trình !\n\n");
                            }

                            break;
                    case 4:
                        if (resultManagement.CountResult() > 0)
                        {
                            int STT;
                            Console.WriteLine("\n\t\t| Xóa kết quả thi đấu |\n");
                            Console.Write("\nNhập STT ket qua thi đấu :");
                            STT = Convert.ToInt32(Console.ReadLine());
                            if (resultManagement.DeletedBySTT(STT))
                            {
                                Console.WriteLine("\nKết quả trận đấu có STT = {0} đã bị xóa ", STT);
                            }
                            else
                            {
                                Console.WriteLine("\nTrận đấu có thể đã bị xóa hoặc không tồn tại !");
                            }
                        }
                        else
                        {
                            Console.Clear();
                            Console.WriteLine("\n\t\tDanh sách kết quả thi đấu trống !\n\n");
                        }
                        Console.Write("\t\tBạn có muốn tiếp tục không ? (Y/N) :");
                            answer = Convert.ToString(Console.ReadLine());
                            if (answer == "Y" || answer == "y")
                            {
                                Console.Clear();
                                goto case 4;
                            }
                            else if (answer == "N" || answer == "n")
                            {
                                Console.Clear();
                            }
                            else
                            {
                                Console.Clear();
                                Console.WriteLine("\n\t\tBạn đã nhập sai => Tự động thoát chương trình !\n\n");
                            }

                            break;
                    
                    case 5:

                        Console.Clear();
                        break;

                    case 0:
                        Console.Clear();
                        menuMain();
                        break;
                    default:
                        Console.Clear();
                        Console.WriteLine("\n\n\t\t\tChức năng không hợp lệ | Mời nhập lại !\n\n");
                        break;
                }
            }
        }


            static void NhapTeam(Team[] team, int n)
            {
                for (int i = 0; i < n; i++)
                {
                    Console.WriteLine("\n\t\t| Đội {0} |", i + 1);
                    Console.Write("\nNhập tên đội :");
                    team[i].name = Console.ReadLine();
                    Console.Write("\nNhập mã đội :");
                    team[i].ID = Console.ReadLine();
                    Console.Write("\nNhập số trận thắng :");
                    team[i].win = Convert.ToInt32(Console.ReadLine());
                    Console.Write("\nNhập số trận hòa :");
                    team[i].draw = Convert.ToInt32(Console.ReadLine());
                    Console.Write("\nNhập số trận thua thua :");
                    team[i].lose = Convert.ToInt32(Console.ReadLine());

                    team[i].point = team[i].win * 3 + team[i].draw * 1 + team[i].lose * 0;
                }
            }
            static void XuatTeam(Team[] team, int n)
            {
                Console.WriteLine("\n\t\t======== BẢNG XẾP HẠNG THÀNH TÍCH V-LEAGUE 2021 ===========\n");
                Console.WriteLine("\t=================================================================================\n");
                Console.WriteLine("\t|  {0,-5}  |  {1,-20}  |  {2,-5}  |  {3,-5}  |  {4,-5}  |  {5,-7}  |\n", "Mã đội", "Tên đội", "Thắng", "Hòa", "Thua", "Tổng điểm");
                Console.WriteLine("\t=================================================================================\n");


                for (int i = 0; i < n; i++)
                {
                    Console.WriteLine("\t|  {0,-5}  |  {1,-20}  |  {2,-5}  |  {3,-5}  |  {4,-5}  |  {5,-5}  |\n", team[i].ID, team[i].name, team[i].win, team[i].draw, team[i].lose, team[i].point);

                }
                Console.WriteLine("\t=================================================================================\n");

            }

            static void SapXep(Team[] team, int n)
            {

                Team temp = new Team();
                for (int i = 0; i < n - 1; i++)
                {
                    for (int j = i + 1; j < n; j++)
                    {
                        if (team[i].point < team[j].point)
                        {
                            temp = team[i];
                            team[i] = team[j];
                            team[j] = temp;
                        }
                    }
                }
            }
        };

       

    }
}
