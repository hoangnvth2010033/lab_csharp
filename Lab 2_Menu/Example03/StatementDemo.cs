﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Example03
{
    class StatementDemo
    {
        public struct Team
        {
            public string name;
            public string ID;
            public int win;
            public int draw;
            public int lose;
            public int point;

           

            static void NhapTeam(Team[] team, int n)
            {
                for (int i = 0; i < n; i++)
                {
                    Console.WriteLine("\n\t\t| Doi {0} |", i + 1);
                    Console.Write("\nNhap ten doi :");
                    team[i].name = Console.ReadLine();
                    Console.Write("\nNhap ma doi :");
                    team[i].ID = Console.ReadLine();
                    Console.Write("\nNhap so tran thang :");
                    team[i].win = Convert.ToInt32(Console.ReadLine());
                    Console.Write("\nNhap so tran hoa :");
                    team[i].draw = Convert.ToInt32(Console.ReadLine());
                    Console.Write("\nNhap so tran thua :");
                    team[i].lose = Convert.ToInt32(Console.ReadLine());

                    team[i].point = team[i].win * 3 + team[i].draw * 1 + team[i].lose * 0;
                }
            }
            static void XuatTeam(Team[] team, int n)
            {
                Console.WriteLine("\n\t\t======== BANG THONG KE THANH TICH V-LEAGUE ===========\n");
                Console.WriteLine("\t=================================================================================\n");
                Console.WriteLine("\t|  {0,-5}  |  {1,-20}  |  {2,-5}  |  {3,-5}  |  {4,-5}  |  {5,-7}  |\n", "Ma doi", "Ten doi", "Thang", "Hoa", "Thua", "Tong diem");
                Console.WriteLine("\t=================================================================================\n");


                for (int i = 0; i < n; i++)
                {
                    Console.WriteLine("\t|  {0,-5}  |  {1,-20}  |  {2,-5}  |  {3,-5}  |  {4,-5}  |  {5,-5}  |\n", team[i].ID, team[i].name, team[i].win, team[i].draw, team[i].lose, team[i].point);

                }
                Console.WriteLine("\t=================================================================================\n");

            }

            static void SapXep(Team[] team, int n)
            {
                
                Team temp = new Team();
                for (int i = 0; i < n - 1; i++)
                {
                    for (int j = i + 1; j < n; j++)
                    {
                        if (team[i].point < team[j].point)
                        {
                            temp = team[i];
                            team[i] = team[j];
                            team[j] = temp;
                        }
                    }
                }
            }
       
        static void Main(string[] args)
        {
                Team[] team = new Team[1000];
                
                int n;
                do
                {
                    Console.Write("\n\n\t\tNhap so doi bong :");
                    n = Convert.ToInt32(Console.ReadLine());
                } while (n < 0 || n > 100);

                NhapTeam(team, n);
                SapXep(team, n);
                XuatTeam(team, n);

            }
        };
    }
}
