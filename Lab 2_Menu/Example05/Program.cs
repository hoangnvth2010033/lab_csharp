﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;
using static System.Convert;

namespace Example05
{
    class Program
    {
        static void Main(string[] args)
        {
            WriteLine("Learn C# day 05");
            int userInput;
            do
            {
                userInput = DisplayMenu();
                switch (userInput)
                {
                    case 1:
                        Clear();
                        PressAnyKey();
                        break;
                    case 2:
                        break;
                }

            } while (userInput!=5);
        }
        private static void PressAnyKey() {
            WriteLine("Press any key...");
            ReadLine();
            Clear();
        }
        private static int DisplayMenu() {
            return ToInt32(SelectedMenu());
                }
        private static int SelectedMenu() {
            WriteLine("Example - Learning C#");
            WriteLine();
            WriteLine("1. Class and NameSpaces");
            WriteLine("2. Console Operator");
            WriteLine("3. Array example");
            WriteLine("4. Collection example");
            WriteLine("5. Exit");
            var result = ReadLine();
            return string.IsNullOrEmpty(result) ? 0 : ToInt32(result);
        }

        private static void LoopAndArrayExample()
        {
            DateTime now = DateTime.Now;
            Random rand = new Random((int)now.Millisecond);

            int[] Arr = new int[10];
            for(int x = 0; x < Arr.Length; ++x)
            {
                Arr[x] = rand.Next() % 100;
            }
            int Total = 0;
            WriteLine("Array calues are : ");
            foreach(int val in Arr)
            {
                Total += val;
                // WriteLine($"{val}");
                Write(val + ",");
            }
            WriteLine("\nAnd the average is {0,0:F1}", (double)Total / (double)Arr.Length);
            ReadLine();

        }
    }
    class ObjectArray
    {
        public void Run()
        {
            int[] intArray;
            Employee[] empArray;
            intArray = new int[5];
            empArray = new Employee[3];

            for(int i = 0; i < empArray.Length; i++)
            {
                empArray[i] = new Employee(i + 5);
            }
            Console.WriteLine("The int array...");
            for (int i = 0; i < intArray.Length; i++)
            {
                Console.WriteLine(intArray[i].ToString());
            }
            Console.WriteLine("The Employee array...");
            for (int i = 0; i < empArray.Length; i++)
            {
                Console.WriteLine(empArray[i].ToString());
            }
           
        }
        static void Main(string[] args)
        {
            ObjectArray arr = new ObjectArray();
            arr.Run();
            
        }
    }
}
