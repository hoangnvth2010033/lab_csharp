﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoItYourSelf
{
    class SINValidator
    {
        static void Main(string[] args)
        {
            int[] Arr = new int[15];
            int[] Arr1 = new int[9];
            int[] Arr2 = new int[9];
            int n1 = 0, n2 = 0;
            int[] sumCheck = new int[9];
            
            Console.WriteLine("\tSIN Validator");
            Console.WriteLine("========================");
            Console.WriteLine("Enter your SIN (0 to quit ) : ");
            for(int i = 1; i <= 9; i++)
            {
                Console.Write(" ");
                Arr[i] = int.Parse(Console.ReadLine());
            }
            int checkDigit = Arr[1];
            Console.Write("\tYour SIN is :");
            
            for (int i = 1; i <= 9; i++)
            {              
                Console.Write(Arr[i] + " ");
                checkDigit = Arr[9];
            }
            Console.WriteLine("\n");
            
           for (int i = 1; i <= 9; i++)
            {
                if (i % 2 == 0)
                {
                    Arr1[n1] = Arr[i];
                    n1++;
                }
                else
                {
                    Arr2[n2] = Arr[i];
                    n2++;
                }
            }

           //Mang check
           for(int i = 0; i < n1; i++)
            {
                Console.Write("{0} ", Arr1[i]);
            }
            Console.WriteLine("\n");

            //mang sau
            for (int i = 0; i < n2; i++)
            {
                Console.Write("{0} ", Arr2[i]);
            }

            for(int i = 0; i < n1; i++)
            {
                Arr1[i] = Arr1[i] * 2;
            }
            
            int sum = 0;
            for(int i = 0; i < n1; i++)
            {             
                    sumCheck[i] = sumCheck[i] + Arr1[i] % 10;
                    Arr1[i] /= 10;
               
                sum += sumCheck[i];
            }
            

            Console.WriteLine("{0} ", sum);
        }


    }
}
