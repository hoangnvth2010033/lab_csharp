﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoYourself_EmployeeSystem
{
    public class EmployeeManagement
    {
        public List<Employee> ListEmployee = null;

        public EmployeeManagement()
        {
            ListEmployee = new List<Employee>();
        }

        //Ham tao STT cho nhan vien
        public int GenerateID()
        {
            int max = 1;
            if(ListEmployee != null && ListEmployee.Count > 0)
            {
                max = ListEmployee[0].STT;
                foreach(Employee emp in ListEmployee)
                {
                    if (max < emp.STT)
                    {
                        max = emp.STT;
                    }
                }
                max++;
            }
            return max;
        }

        //Ham dem so nhan vien
        public int CountEmployee()
        {
            int count = 0;
            if (ListEmployee != null)
            {
                count = ListEmployee.Count;
            }
            return count;
        }

        //Ham tim theo ten nhan vien
        public Employee FindByName(string name)
        {
            Employee searchName = null;
            if(ListEmployee != null && ListEmployee.Count > 0)
            {
                foreach(Employee employee in ListEmployee)
                {
                    if(employee.name == name)
                    {
                        searchName = employee;
                    }
                }
            }
            return searchName;
        }

        //Ham them moi nhan vien 
        public void CreateEmployee()
        {
            Employee emp = new Employee();
            emp.STT = GenerateID();

            Console.Write("Nhap ten nhan vien : ");
            emp.name = Console.ReadLine();

            Console.Write("Nhap do tuoi : ");
            emp.age = Convert.ToInt32(Console.ReadLine());

            Console.Write("Nhap muc luong : ");
            emp.salary = Convert.ToDouble(Console.ReadLine());

            ListEmployee.Add(emp);
        }

        // Ham xoa theo ten nhan vien 
        public bool DeleteByName(string empName)
        {
            bool isDeleted = false;
            Employee employee = FindByName(empName);
            if(employee != null)
            {
                isDeleted = ListEmployee.Remove(employee);
            }
            return isDeleted;
        }

        //Ham cap nhat nhan vien theo ten
        public void updateEmployee(string empName)
        {
            Employee employee = FindByName(empName);

            if(employee != null)
            {
                Console.Write("Sua tuoi NV : ");
                string age = Convert.ToString(Console.ReadLine());
                if(age != null && age.Length > 0)
                {
                    employee.age = Convert.ToInt32(age);
                }

                Console.Write("Sua muc luong NV : ");
                string salary = Convert.ToString(Console.ReadLine());
                if (salary != null && salary.Length > 0)
                {
                    employee.salary = Convert.ToDouble(salary);
                }
            }
            else
            {
                Console.WriteLine("\n\nt\t\t Nhan vien co ten ' {0} ' khong ton tai !\n\n", empName);
            }
        }

        //Ham hien thi danh sach nhan vien
        public void ShowEmployee(List<Employee> employees)
        {
            Console.WriteLine("{0, -5} | {1, -15} | {2, -25} | {3, -15} |", "STT", "Ten NV", "Tuoi", "Muc luong");
            if(employees != null && employees.Count > 0)
            {
                foreach(Employee emp in employees)
                {
                    Console.WriteLine("{0, -5} | {1, -15} | {2, -25} | {3, -15} |", emp.STT, emp.name, emp.age, emp.salary);
                }
            }
            Console.WriteLine();
        }

        public List<Employee> GetEmployees()
        {
            return ListEmployee;
        }

    }
}
