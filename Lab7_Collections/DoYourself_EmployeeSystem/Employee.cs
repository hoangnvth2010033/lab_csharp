﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoYourself_EmployeeSystem
{
    public class Employee
    {
        public int STT { get; set; }
        public string name { get; set; }
        public int age { get; set; }
        public double salary { get; set; }
    }
}
