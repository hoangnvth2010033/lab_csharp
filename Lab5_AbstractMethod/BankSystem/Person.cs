﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankSystem
{
    public abstract class Person
    {
        protected string id;
        protected string name;
        protected string phone;
      
        protected Person(string name,string id,string phone)
        {
            this.id = id;
            this.name = name;
            this.phone = phone;
        }
        protected Person()
        {
            name = "demo";
            phone = "4234-433-432";
            id = "CUST01";
        }

      public string pName
        {
            get { return name; }
            set { name = value; }
        }
        public string pID
        {
            get { return id; }
            set { id = value; }
        }
        public string pPhone
        {
            get { return phone; }
            set { phone = value; }
        }

     
    }
}
