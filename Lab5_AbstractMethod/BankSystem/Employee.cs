﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankSystem
{

    public class InvalidWithdraw : ApplicationException
    {
        public InvalidWithdraw() : base("Your account is not enough to withdraw ! ") { }
    }
    public abstract class Employee:Person
    {
        protected string email;
        private string role;
        List<Customer> customers = new List<Customer>();
            

        protected Employee()
        {           
            email = "demo";
            role = "demo";
        }

        public Employee( string email, string role)
        {
            this.email = email;
            this.role = role;
        }

        public Employee(string e, string r, string n, string p, string i) : base(n, p, i)
        {
            this.email = e;
            this.role = r;
        }

        public void createCustAccount()
        {

        }

    }
}
