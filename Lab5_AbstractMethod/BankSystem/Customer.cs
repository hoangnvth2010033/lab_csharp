﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankSystem
{
    public class Customer:Person
    {
        protected string address;
        protected string email;
        private decimal balance;

        protected Customer()
        {
            address = "demo";
            email = "demo";
            balance = 20000;
        }

        public Customer(string address,string email,decimal balance)
        {
            this.address = address;
            this.email = email;
            this.balance = balance;
        }

        public Customer(string a,string e,decimal b,string n,string p,string i) : base(n, p, i)
        {
            this.address = a;
            this.email = e;
            this.balance = b;
        }
        public void Deposit()
        {
            Console.WriteLine("Enter amount you want to deposit : ");
            decimal amount = Convert.ToDecimal(Console.ReadLine());
            balance += amount;
        }
        public void Withdraw()
        {
            decimal amount = 0;
            do
            {
                Console.WriteLine("Enter amount you want to deposit : ");
                 amount = Convert.ToDecimal(Console.ReadLine());
                if (amount > balance)
                {
                    Console.WriteLine("\n\tAmount you want to withdraw is over your balance ! Please try again !\n");
                }
            } while (amount > balance);

                  balance -= amount;
        }

        public override string ToString()
        {
            return "\n\t\t Customer : \n\tCustomer ID : " + id + "\n\tName : " + name + "\n\tPhone number : " + phone +
                "\n\t Address : " + address + "\n\tEmail : " + email + "\n\tBalance : " + balance;
        }


    }
}
