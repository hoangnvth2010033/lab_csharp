﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise_5
{
    class Factorials
    {
        static void Main(string[] args)
        {
            for(int i = 1; i <= 20; i++)
            {
                Console.WriteLine("\nFactorial of {0} is {1}",i, Factorial(i));
            }
        }
        static long Factorial(int n)
        {
            if (n == 1)
            {
                return 1;
            }
            else
            {
                return n * Factorial(n - 1);
            }
        }
    }
}
