﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise_3
{
    class DisplayDay
    {
        static void Main(string[] args)
        {
            int choice;
            Console.WriteLine("Enter your lucky number ( from 1 to 7 ) :");
            choice = Convert.ToInt32(Console.ReadLine());

            switch (choice)
            {
                case 1:
                    Console.WriteLine("Monday is your day !");
                    break;
                case 2:
                    Console.WriteLine("Tuesday is your day !");
                    break;
                case 3:
                    Console.WriteLine("Wednesday is your day !");
                    break;
                case 4:
                    Console.WriteLine("Thursday is your day !");
                    break;
                case 5:
                    Console.WriteLine("Friday is your day !");
                    break;
                case 6:
                    Console.WriteLine("Saturday is your day !");
                    break;
                case 7:
                    Console.WriteLine("Sunday is your day !");
                    break;
                default:
                    Console.WriteLine("Oops! Wrong number");
                    break;

            }
        }
    }
}
