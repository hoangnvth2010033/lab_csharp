﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise_4
{
    class multiplicationTable
    {
        static void Main(string[] args)
        {
            int N;
            Console.Write("Enter N :");
            N = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("\t\t==== Multiplication Table of N ==== \t\t");
            for(int i = 1; i <= 9; i++)
            {
                Console.WriteLine("\t\t\t {0} x {1} = {2} \n", N, i, N * i);
            }
        }
    }
}
