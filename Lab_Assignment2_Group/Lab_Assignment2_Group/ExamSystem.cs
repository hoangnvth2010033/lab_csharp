﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_Assignment2_Group
{
    class ExamSystem
    {
        QuestionAnswerManagement qaManagement = new QuestionAnswerManagement();
        ExamManagement examManagement = new ExamManagement();
        public static void Main(string[] args)
        {
            ExamSystem exam = new ExamSystem();
            Console.InputEncoding = Encoding.UTF8;
            Console.OutputEncoding = Encoding.UTF8;
            exam.menuMain();
        }

        static void PressAnyKey()
        {
            Console.WriteLine("\n\t\tNhấn phím bất kì để tiếp tục...");
            Console.ReadLine();
            Console.Clear();
        }

        public void menuMain()
        {
            int key;

            while (true)
            {
                Console.WriteLine("\n\t\t\t   CHƯƠNG TRÌNH QUẢN LÝ ĐỀ THI  \n");
                Console.WriteLine("\t\t ================== MAIN MENU ===================");           
                Console.WriteLine("\t\t|          1. Quản lý câu hỏi/trả lời.           |");
                Console.WriteLine("\t\t|          2. Quản lý đề thi.                    |");
                Console.WriteLine("\t\t|          0. Thoát.                             |");
                Console.WriteLine("\t\t ================================================");
                try
                {
                    Console.Write("\n\t# Nhập lựa chọn : ");
                    key = Convert.ToInt32(Console.ReadLine());

                    switch (key)
                    {
                        case 1:
                            Console.Clear();
                            menuQuanLyCauHoi();
                            Console.Clear();
                            break;

                        case 2:
                            Console.Clear();
                            menuQuanLyDeThi();
                            Console.Clear();
                            break;

                        case 0:
                            Console.WriteLine("\n\n\t\t \u263A \u263A \u263A Goodbye !! \u263A \u263A \u263A\n\n");
                            Environment.Exit(0);
                            break;

                        default:
                            Console.Clear();
                            Console.WriteLine("\n\n\t\t\tChức năng không hợp lệ | Mời nhập lại !\n\n");
                            break;

                    }
                }catch(Exception error)
                {
                    Console.Clear();
                    Console.WriteLine("\n\t\t\t Kiểu giá trị nhập vào không hợp lệ !!");
                    Console.WriteLine("\t\t\t=> " + error.Message);
                }
            }
        }

        public void menuQuanLyCauHoi()
        {
            int key;
            string answer;
            while (true)
            {
                Console.WriteLine("\n\n\t\t -----------  QUẢN LÝ CÂU HỎI/TRẢ LỜI  ----------");
                Console.WriteLine("\t\t|------------------------------------------------|");
                Console.WriteLine("\t\t|          1. Xem danh sách Q/A                  |");
                Console.WriteLine("\t\t|          2. Cập nhật Q/A                       |");
                Console.WriteLine("\t\t|          3. Tạo mới một Q/A                    |");
                Console.WriteLine("\t\t|          4. Xóa Q/A                            |");
                Console.WriteLine("\t\t|          0. Trở về menu chính                  |");
                Console.WriteLine("\t\t ================================================");
                try
                {
                    Console.Write("\n\t# Nhập lựa chọn : ");
                    key = Convert.ToInt32(Console.ReadLine());

                    switch (key)
                    {
                        case 1:
                            if (qaManagement.CountQuestion() > 0)
                            {
                                Console.WriteLine("\n\t\t| Xem danh sách câu hỏi |\n");
                                qaManagement.ShowQuestion(qaManagement.GetQuestionAnswers());
                            }
                            else
                            {
                                Console.Clear();
                                Console.WriteLine("\n\t\t Danh sách câu hỏi trống ! \n\n");
                            }
                            PressAnyKey();
                            break;

                        case 2:
                            if (qaManagement.CountQuestion() > 0)
                            {
                                string  questID;
                                Console.WriteLine("\n\t\t| Cập nhật câu hỏi |\n");                           
                                Console.Write("\nNhập mã câu hỏi : ");
                                questID = Console.ReadLine();
                                qaManagement.updateQuestion(questID);
                            }
                            else
                            {
                                Console.WriteLine("\n\t\tDanh sách câu hỏi trống !\n\n");
                            }
                            Console.Write("\t\tBạn có muốn tiếp tục không ? (Y/N) :");
                            answer = Convert.ToString(Console.ReadLine());
                            if (answer == "Y" || answer == "y")
                            {
                                Console.Clear();
                                goto case 2;
                            }
                            else if (answer == "N" || answer == "n")
                            {
                                Console.Clear();
                            }
                            else
                            {
                                Console.Clear();
                                Console.WriteLine("\n\t\tBạn đã nhập sai => Tự động thoát chương trình !\n\n");
                            }
                            break;

                        case 3:
                            Console.WriteLine("\n\t\t| Thêm mới câu hỏi |\n");
                            qaManagement.CreateQuestion();
                            Console.Write("\t\tBạn có muốn tiếp tục không ? (Y/N) :");
                            answer = Convert.ToString(Console.ReadLine());
                            if (answer == "Y" || answer == "y")
                            {
                                Console.Clear();
                                goto case 3;
                            }
                            else if (answer == "N" || answer == "n")
                            {
                                Console.Clear();
                            }
                            else
                            {
                                Console.Clear();
                                Console.WriteLine("\n\t\tBạn đã nhập sai => Tự động thoát chương trình !\n\n");
                            }
                            break;

                        case 4:
                            if (qaManagement.CountQuestion() > 0)
                            {
                                string id;
                                Console.WriteLine("\n\t\t| Xóa câu hỏi |\n");
                                Console.Write("\nNhập mã câu hỏi : ");
                                id = Console.ReadLine();
                                if (qaManagement.DeleteByID(id))
                                {
                                    Console.WriteLine("\nCâu hỏi có ID = {0} đã bị xóa \n\n", id);
                                }
                                else
                                {
                                    Console.WriteLine("\nCâu hỏi có thể đã bị xóa hoặc không tồn tại !\n\n");
                                }
                            }
                            else
                            {
                                Console.Clear();
                                Console.WriteLine("\n\t\tDanh sách câu hỏi trống !\n\n");
                            }
                            Console.Write("\t\tBạn có muốn tiếp tục không ? (Y/N) :");
                            answer = Convert.ToString(Console.ReadLine());
                            if (answer == "Y" || answer == "y")
                            {
                                Console.Clear();
                                goto case 4;
                            }
                            else if (answer == "N" || answer == "n")
                            {
                                Console.Clear();
                            }
                            else
                            {
                                Console.Clear();
                                Console.WriteLine("\n\t\tBạn đã nhập sai => Tự động thoát chương trình !\n\n");
                            }
                            break;

                        case 0:
                            Console.Clear();
                            menuMain();
                            break;

                        default:
                            Console.Clear();
                            Console.WriteLine("\n\n\t\t\tChức năng không hợp lệ | Mời nhập lại !\n\n");
                            break;
                    }
                }
                catch (Exception error)
                {
                    Console.Clear();
                    Console.WriteLine("\n\t\t\t Kiểu giá trị nhập vào không hợp lệ !!");
                    Console.WriteLine("\t\t\t=> " + error.Message);
                }
            }
        }

        public void menuQuanLyDeThi()
        {
            int key,subKey;
            string answer;
            while (true)
            {
                Console.WriteLine("\n\n\t\t ---------------  QUẢN LÝ ĐỀ THI  ---------------");
                Console.WriteLine("\t\t|------------------------------------------------|");
                Console.WriteLine("\t\t|            1. Xem đề thi                       |");
                Console.WriteLine("\t\t|            2. Tạo đề thi                       |");
                Console.WriteLine("\t\t|            0. Trở về menu chính                |");
                Console.WriteLine("\t\t ================================================");
                try
                {
                    Console.Write("\n\t# Nhập lựa chọn : ");
                    key = Convert.ToInt32(Console.ReadLine());
                    switch (key)
                    {
                        case 1:
                            if (examManagement.CountExamn() > 0)
                            {
                                Console.WriteLine("\n\t\t| Xem đề thi |\n");
                                Console.Write("\n\n\t\tNhập tên file đề thi : ");
                                string file = Convert.ToString(Console.ReadLine());
                                examManagement.ShowExam(examManagement.GetExams(),file);
                            }
                            else
                            {
                                Console.Clear();
                                Console.WriteLine("\n\t\tDanh sách đề thi trống !\n\n");

                            }
                            Console.Write("\t\tBạn có muốn tiếp tục không ? (Y/N) :");
                            answer = Convert.ToString(Console.ReadLine());
                            if (answer == "Y" || answer == "y")
                            {
                                Console.Clear();
                                goto case 1;
                            }
                            else if (answer == "N" || answer == "n")
                            {
                                Console.Clear();
                            }
                            else
                            {
                                Console.Clear();
                                Console.WriteLine("\n\t\tBạn đã nhập sai => Tự động thoát chương trình !\n\n");
                            }


                            break;

                        case 2:
                            Console.Clear();
                            while (true)
                            {
                                Console.WriteLine("\n\n\t\t ----------------  TẠO ĐỀ THI  ------------------");
                                Console.WriteLine("\t\t|------------------------------------------------|");
                                Console.WriteLine("\t\t|       1. Đề thi theo số lượng câu hỏi          |");
                                Console.WriteLine("\t\t|       2. Đề thi theo tổng số điểm              |");
                                Console.WriteLine("\t\t|       0. Trở về menu trước                     |");
                                Console.WriteLine("\t\t ================================================");
                                try {
                                    Console.Write("\n\t# Nhập lựa chọn : ");
                                    subKey = Convert.ToInt32(Console.ReadLine());

                                    switch (subKey)
                                    {
                                        case 1:
                                            if (qaManagement.CountQuestion() > 0) {
                                                Console.WriteLine("\n\t\t| Tạo đề thi theo số lượng câu hỏi |\n");
                                                examManagement.CreateExam(qaManagement.GetQuestionAnswers());
                                            }
                                            else
                                            {
                                                Console.Clear();
                                                Console.WriteLine("\n\t\tDanh sách câu hỏi trống !\n\n");

                                            }
                                            Console.Write("\t\tBạn có muốn tiếp tục không ? (Y/N) :");
                                            answer = Convert.ToString(Console.ReadLine());
                                            if (answer == "Y" || answer == "y")
                                            {
                                                Console.Clear();
                                                goto case 1;
                                            }
                                            else if (answer == "N" || answer == "n")
                                            {
                                                Console.Clear();
                                            }
                                            else
                                            {
                                                Console.Clear();
                                                Console.WriteLine("\n\t\tBạn đã nhập sai => Tự động thoát chương trình !\n\n");
                                            }
                                            break;

                                        case 2:
                                            Console.Clear();
                                            break;

                                        case 0:
                                            Console.Clear();
                                            menuQuanLyDeThi();
                                            break;

                                        default:
                                            Console.Clear();
                                            Console.WriteLine("\n\n\t\t\tChức năng không hợp lệ | Mời nhập lại !\n\n");
                                            break;
                                    }
                                }
                                catch(Exception error)
                                {
                                    Console.Clear();
                                    Console.WriteLine("\n\t\t\t Kiểu giá trị nhập vào không hợp lệ !!");
                                    Console.WriteLine("\t\t\t=> " + error.Message);
                                }
                            }
                                
                        case 0:
                            Console.Clear();
                            menuMain();
                            break;

                        default:
                            Console.Clear();
                            Console.WriteLine("\n\n\t\t\tChức năng không hợp lệ | Mời nhập lại !\n\n");
                            break;
                    }
                }
                catch(Exception error)
                {
                    Console.Clear();
                    Console.WriteLine("\n\t\t\t Kiểu giá trị nhập vào không hợp lệ !!");
                    Console.WriteLine("\t\t\t=> " + error.Message);
                }

               
            }
        }

    }
}
