﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_Assignment2_Group
{
      public class QuestionAnswer
    {
        public int STT { get; set; }
        public string categoryID { get; set; }
        public string field { get; set; }
        public string questionID { get; set; }
        public string questionContent { get; set; }
        public double point { get; set; }
        public string  answerA { get; set; }
        public string answerB { get; set; }
        public string answerC { get; set; }
        public string answerD { get; set; }
        public string answerValueA { get; set; }
        public string answerValueB { get; set; }

        public string answerValueC { get; set; }
        public string answerValueD { get; set; }

    }
}
