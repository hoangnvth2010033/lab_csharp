﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_Assignment2_Group
{
    public class QuestionAnswerManagement
    {
        public List<QuestionAnswer> ListQuestion = null;

        // ==================== QUESTION MANAGEMENT ============================
        public QuestionAnswerManagement()
        {
            ListQuestion = new List<QuestionAnswer>();
        }

        //Ham tao STT tang dan cho cau hoi
        public int GenerateID()
        {
            int max = 1;
            if (ListQuestion != null && ListQuestion.Count > 0)
            {
                max = ListQuestion[0].STT;
                foreach (QuestionAnswer qa in ListQuestion)
                {
                    if (max < qa.STT)
                    {
                        max = qa.STT;
                    }
                }
                max++;
            }
            return max;
        }

        //Ham dem so cau hoi
        public int CountQuestion()
        {
            int count = 0;
            if (ListQuestion != null)
            {
                count = ListQuestion.Count;
            }
            return count;
        }

        //Ham tim theo ma danh muc
        public QuestionAnswer FindByCategoryID(string category)
        {
            QuestionAnswer searchID = null;
            if (ListQuestion != null && ListQuestion.Count > 0)
            {
                foreach (QuestionAnswer question in ListQuestion)
                {
                    if (question.categoryID == category)
                    {
                        searchID = question;
                    }
                }
            }
            return searchID;
        }

        //Ham tim theo ma cau hoi
        public QuestionAnswer FindByQuestionID(string questID)
        {
            QuestionAnswer searchID = null;
            if (ListQuestion != null && ListQuestion.Count > 0)
            {
                foreach (QuestionAnswer question in ListQuestion)
                {
                    if (question.questionID == questID)
                    {
                        searchID = question;
                    }
                }
            }
            return searchID;
        }

        //Ham them moi cau hoi
        public void CreateQuestion()
        {
            QuestionAnswer qa = new QuestionAnswer();
            qa.STT = GenerateID();

            Console.Write("Nhập mã danh mục :");
            qa.categoryID = Convert.ToString(Console.ReadLine());

            Console.Write("Câu hỏi thuộc lĩnh vực :");
            qa.field = Convert.ToString(Console.ReadLine());

            Console.Write("Nhập mã câu hỏi :");
            qa.questionID = Convert.ToString(Console.ReadLine());

            Console.Write("Nhập câu hỏi :");
            qa.questionContent = Convert.ToString(Console.ReadLine());

            Console.Write("Điểm số :");
            qa.point = Convert.ToDouble(Console.ReadLine());

            Console.Write("Đáp án A :");
            qa.answerA = Console.ReadLine();
            Console.Write("Đúng/Sai ? (D/S) : ");
            qa.answerValueA = Console.ReadLine();

            Console.Write("Đáp án B :");
            qa.answerB = Console.ReadLine();
            Console.Write("Đúng/Sai ? (D/S) : ");
            qa.answerValueB = Console.ReadLine();

            Console.Write("Đáp án C :");
            qa.answerC = Console.ReadLine();
            Console.Write("Đúng/Sai ? (D/S) : ");
            qa.answerValueC = Console.ReadLine();

            Console.Write("Đáp án D :");
            qa.answerD = Console.ReadLine();
            Console.Write("Đúng/Sai ? (D/S) : ");
            qa.answerValueD = Console.ReadLine();

            Console.Write("\n\n\t\t\tBạn muốn lưu câu hỏi này ? (Y/N) : ");
            string save = Convert.ToString(Console.ReadLine());
            if (save == "Y" || save == "y" || save == "yes" || save == "YES")
            {
                Console.Clear();
                ListQuestion.Add(qa);
                Console.WriteLine("\n\n\t\tThêm câu hỏi thành công !\n");
            }
            else if (save == "N" || save == "n" || save == "no" || save == "NO")
            {
                Console.Clear();
                Console.WriteLine("\n\n\t\t\t Câu hỏi không được lưu !\n\n");
            }
            else
            {
                Console.Clear();
                Console.WriteLine("\n\n\t\t\t Tự động hủy Tạo mới !\n\n");
            }
        }

        //Ham xoa cau hoi theo ma danh muc
        public bool DeleteByID(string id)
        {
            bool IsDeleted = false;
            QuestionAnswer questionID = FindByQuestionID(id);
            if (questionID != null)
            {
                IsDeleted = ListQuestion.Remove(questionID);
            }
            return IsDeleted;
        }


        //Ham cap nhat cau hoi
        public void updateQuestion(string questionID)
        {
            QuestionAnswer question = FindByQuestionID(questionID);

            if (question != null)
            {
                Console.WriteLine("\n\t Đây là câu hỏi thuộc lĩnh vực {0}.\n", question.field);

                Console.Write("Sửa nội dung câu hỏi : ");
                string quest = Convert.ToString(Console.ReadLine());
                if (quest != null && quest.Length > 0)
                {
                    question.questionContent = quest;

                }

                Console.Write("Sửa điểm số : ");
                string newPoint = Convert.ToString(Console.ReadLine());
                if (newPoint != null && newPoint.Length > 0)
                {
                    question.point = Convert.ToDouble(newPoint);

                }

                Console.Write("Sửa đáp án A : ");
                string newA = Console.ReadLine();
                if (newA != null && newA.Length > 0)
                {
                    question.answerA = newA;

                }
                Console.Write("Đúng/Sai/Xóa ? (D/S/X) : ");
                string newValA = Console.ReadLine(); 
                if(newValA=="X" || newValA == "x")
                {
                    question.answerA = "Deleted ";
                    question.answerValueA = " ";
                }
                else if (newValA != null && newValA.Length > 0)
                {
                    question.answerValueA = newValA;
                }

                Console.Write("Sửa đáp án B : ");
                string newB = Console.ReadLine();
                if (newB != null && newB.Length > 0)
                {
                    question.answerB = newB;

                }
                Console.Write("Đúng/Sai/Xóa ? (D/S/X) : ");
                string newValB = Console.ReadLine();
                if(newValB=="X" || newValB == "x")
                {
                    question.answerB = "Deleted ";
                    question.answerValueA = " ";
                }
                else if (newValB != null && newValB.Length > 0)
                {
                    question.answerValueB = newValB;
                }

                Console.Write("Sửa đáp án C : ");
                string newC = Console.ReadLine();
                if (newC != null && newC.Length > 0)
                {
                    question.answerC = newC;

                }
                Console.Write("Đúng/Sai/Xóa ? (D/S/X) : ");
                string newValC = Console.ReadLine();
                if(newValC == "X" || newValC == "x")
                {
                    question.answerC = "Deleted";
                    question.answerValueC = " ";
                }
                else if (newValC != null && newValC.Length > 0)
                {
                    question.answerValueC = newValC;
                }

                Console.Write("Sửa đáp án D : ");
                string newD = Console.ReadLine();
                if (newD != null && newD.Length > 0)
                {
                    question.answerD = newD;

                }
                Console.Write("Đúng/Sai/Xóa ? (D/S/X) : ");
                string newValD = Console.ReadLine();
                 if(newValD =="X" || newValD == "x")
                {
                    question.answerValueD = " ";
                    question.answerD = "Deleted";
                }
                else if (newValD != null && newValD.Length > 0)
                {
                    question.answerValueD = newValD;
                }

                Console.WriteLine("\n\n\t\t Cập nhật câu hỏi thành công !");
            }
            else
            {
                Console.WriteLine("\n\n\t\t Câu hỏi có mã  ' {0} ' không tồn tại ! \n\n", questionID);
            }
        }

        //Ham hien thi danh sach cau hoi
        public void ShowQuestion(List<QuestionAnswer> questionAnswers)
        {
            Console.WriteLine("{0, -5} | {1, -15} | {2, -25} | {3, -15} | {4,-8} | {5,-30} | {6,-18} | {7,-18} | {8,-18} | {9,-18} | ",
                "STT", "Mã danh mục", "Lĩnh vực", "Mã câu hỏi", "Điểm số", "Nội dung câu hỏi","A","B","C","D");
           

            if (questionAnswers != null && questionAnswers.Count > 0)
            {
                foreach (QuestionAnswer qa in questionAnswers)
                {
                    Console.WriteLine("{0, -5} | {1, -15} | {2, -25} | {3, -15} | {4,-8} | {5,-30} | {6,-15}({7}) | {8,-15}({9}) | {10,-15}({11}) | {12,-15}({13}) | ",
               qa.STT, qa.categoryID, qa.field, qa.questionID, qa.point, qa.questionContent,qa.answerA,qa.answerValueA,qa.answerB,qa.answerValueB,qa.answerC,qa.answerValueC,qa.answerD,qa.answerValueD);

                }
            }
            Console.WriteLine();
        }

        //Ham tra ve danh sach cau hoi hien tai
        public List<QuestionAnswer> GetQuestionAnswers()
        {
            return ListQuestion;
        }


    }
}

