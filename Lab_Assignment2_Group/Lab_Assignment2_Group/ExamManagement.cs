﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_Assignment2_Group
{
    public class ExamManagement
    {
        QuestionAnswerManagement qaManagement = new QuestionAnswerManagement();
        public List<QuestionAnswer> ListQuestion = null;
        public List<Exam> ListExam = null;

        public ExamManagement()
        {
            ListQuestion = new List<QuestionAnswer>();
            ListExam = new List<Exam>();
        }
        // ==================== EXAM MANAGEMENT ======================================

        //Ham tao de thi theo yeu cau
        public void CreateExam(List<QuestionAnswer> questionAnswers)
        {
            Exam exam = new Exam();

            if (questionAnswers != null && questionAnswers.Count > 0)
            {
                Console.Write("\nNhập số lượng câu hỏi : ");
                int n = Convert.ToInt32(Console.ReadLine());
                if (n-1 > qaManagement.CountQuestion())
                {
                    Console.WriteLine("\n\n\t\tSố lượng câu hỏi không đủ để tạo đề thi !\n\n");
                } 
                else
                {
                    Console.WriteLine("\n\t... Đang tạo đề thi ...");
                    Console.WriteLine("\n\t... Đã tạo xong ... ");
                  
                    foreach (QuestionAnswer quest in questionAnswers)
                    {

                        for (int i = 0; i < n; i++)
                        {

                          //  var quest1 = ListQuestion[i];
                            exam.qaSTT = quest.STT;
                            exam.qaCategoryID = quest.categoryID;
                            exam.qaField = quest.field;
                            exam.qaQuestionID = quest.questionContent;
                            exam.qaQuestionContent = quest.questionContent;
                            exam.qaPoint = quest.point;
                            exam.qaAnswerA = quest.answerA;
                            exam.qaAnswerB = quest.answerB;
                            exam.qaAnswerC = quest.answerC;
                            exam.qaAnswerD = quest.answerD;
                        }
                       
                    } 

                   
                    

                 
                                       

                    Console.Write("\n\tNhập tên file : ");
                    exam.fileName = Convert.ToString(Console.ReadLine());                
                    Console.WriteLine("\n\t... Đang lưu ...");
                    Console.WriteLine("\n\t... Đã lưu xong ...");

                    Console.Write("\n\n\t\t\tBạn muốn lưu đề thi này ? (Y/N) : ");
                    string save = Convert.ToString(Console.ReadLine());
                    if (save == "Y" || save == "y" || save == "yes" || save == "YES")
                    {
                        ListExam.Add(exam);
                        Console.Clear();
                        Console.WriteLine("\n\n\t\tThêm đề thi thành công !\n");
                    }
                    else if (save == "N" || save == "n" || save == "no" || save == "NO")
                    {
                        Console.Clear();
                        Console.WriteLine("\n\n\t\t\t Đề thi không được lưu !");
                    }
                    else
                    {
                        Console.Clear();
                        Console.WriteLine("\n\n\t\t\t Tự động hủy Lưu đề thi !");
                    }
                }            
            }         
        }

        //Ham dem so de thi
        public int CountExamn()
        {
            int count = 0;
            if (ListExam != null)
            {
                count = ListExam.Count;
            }
            return count;
        }

        //Ham tim theo ten file
        public Exam FindByFileName(string file)
        {
            Exam searchFile = null;
            if (ListExam != null && ListExam.Count > 0)
            {
                foreach (Exam ex in ListExam)
                {
                    if (ex.fileName == file)
                    {
                        searchFile = ex;
                    }
                }
            }
            return searchFile;
        }

        //Ham hien thi de thi
        public void ShowExam(List<Exam> listExams ,string file)
        {
            Exam exam = FindByFileName(file);
            List<QuestionAnswer> listQA = new List<QuestionAnswer>();
            if(exam != null)
            {
                if(listExams != null && listExams.Count > 0)
                {
                    foreach (QuestionAnswer qa in listQA)
                    {
                        foreach (Exam exam1 in listExams)
                        {
                            Console.WriteLine("\n\n===============================================");
                            Console.WriteLine("\tCâu {0} : {1}   [{2}đ]", qa.STT, qa.questionContent, qa.point);
                            Console.WriteLine("\tA. {0} ", qa.answerA);
                            Console.WriteLine("\tB. {0} ", qa.answerB);
                            Console.WriteLine("\tC. {0} ", qa.answerC);
                            Console.WriteLine("\tD. {0} ", qa.answerD);
                        }
                    }
                }
                Console.WriteLine();
            }
            else
            {
                Console.WriteLine("\n\n\t\tĐề thi có tên file ' {0} ' không tồn tại !",file);
            }
        }


        //Ham tra ve danh sach de thi hien tai
        public List<Exam> GetExams()
        {
            return ListExam;
        }

    }
}
