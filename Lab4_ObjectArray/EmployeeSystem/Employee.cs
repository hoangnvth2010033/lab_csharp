﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeSystem
{
    class Employee
    {
        protected string fName;
        protected string lName;
        protected string address;
        protected long sin;
        protected double salary;

        public Employee(string fname, string lname, string address, long sin,double salary)
        {
            this.fName = fname;
            this.lName = lname;
            this.address = address;
            this.sin = sin;
            this.salary = salary;
        }

        protected Employee()
        {
            fName = "Micheal";
            lName = "List";
            address = "Hanoi";
            sin = 32423423432;
            salary = 23000;
        }

        public override string ToString()
        {
            return ("\n\t Employee : \n Name : " + fName+lName + "\n Address : " + address +
                "\n Sin : " + sin + "\n Salary : " +salary ) ;
        }

        public double Bonus(float parameter) {
            return salary * parameter;       
         }
    }
}
