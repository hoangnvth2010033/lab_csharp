﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AtomSystem
{
    class Atom
    {
        public int aNumber { get; set; }
        public string aName { get; set; }
        public string aSymbol { get; set; }
        public float aWeight { get; set; }
        
    }
}
