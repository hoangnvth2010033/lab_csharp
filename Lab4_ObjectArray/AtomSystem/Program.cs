﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AtomSystem
{
    class Program
    {
        private List<Atom> ListAtom = null;
        public Program()
        {
            ListAtom = new List<Atom>();
        }

        //Ham them moi nguyen to
        public void CreateAtom()
        {
            Atom at = new Atom();
            Console.Write("Enter atomic number :");
            at.aNumber = Convert.ToInt32(Console.ReadLine());

            Console.Write("Enter symbol :");
            at.aSymbol = Convert.ToString(Console.ReadLine());

            Console.Write("Enter full name :");
            at.aName = Convert.ToString(Console.ReadLine());

            Console.Write("Enter atomic weight :");
            at.aWeight = float.Parse(Console.ReadLine());

            ListAtom.Add(at);
        }

        //Ham hien thi danh sach
        public void Display(List<Atom> atoms)
        {
            Console.WriteLine("{0, -5} | {1, -20} | {2, -20} | {3, -25} | ",
              "Number", "Symbol", "Full Name", "Weight");
            if (atoms != null && atoms.Count > 0)
            {
                foreach (Atom atom in atoms)
                {
                    Console.WriteLine("{0, -5} | {1, -20} | {2, -20} | {3, -25}  ",
               atom.aNumber,atom.aSymbol,atom.aName,atom.aWeight);

                }
            }
            Console.WriteLine();
        }
        //Ham tr ve danh sach
        public List<Atom> GetAtoms()
        {
            return ListAtom;
        }
        static void Main(string[] args)
        {
            Program pr = new Program();
            int n;
            do
            {
                Console.WriteLine("\t\tEnter the number of atoms :");
                n = Convert.ToInt32(Console.ReadLine());
            } while (n < 0 || n >= 100);
           
            for(int i = 0; i < n; i++)
            {
                Console.WriteLine("Atom [{0}]", i + 1);
                pr.CreateAtom();
                Console.WriteLine("\n");
            }
            Console.WriteLine("\n\tAtomic Information");
            Console.WriteLine("===============================\n");
            pr.Display(pr.GetAtoms());
        }
    }
}
