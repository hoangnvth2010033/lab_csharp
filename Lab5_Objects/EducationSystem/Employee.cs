﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EducationSystem
{
   public abstract class Employee : Person
    {
        protected string department;
        protected double salary;
        protected string dateHired;

        protected Employee()
        {
            department = "A1 building";
            salary = 12000;
            dateHired = "12/4/2344";
        }

        public Employee(string department, double salary, string hired)
        {
            this.department = department;
            this.salary = salary;
            this.dateHired = hired;
        }
        

        public Employee(string d, double s,string dt, string n,string p,string e) : base(n, p, e)
        {
            this.department = d;
            this.salary = s;
            this.dateHired = dt;
        }

        public override string ToString()
        {
            return "\n\t Student : \n Name : " + pName + "\n Phone Number : " + phoneNo +
                "\n Email : " + email + "\n Department : " + department + "\n Salary : " +salary +" $"+ "\n Date Hired : "+dateHired ;

        }

        public abstract double CalculateBonus();
        public abstract double CalculateVacation();
    }
   
}
