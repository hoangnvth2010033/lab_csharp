﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EducationSystem
{
    class Faculty : Employee
    {
        private string officeHours;
        private string rank;
        public Faculty(string o, string r)
        {
            this.officeHours = o;
            this.rank = r;
        }

        public Faculty(string o, string r,string department, double salary,string date, string name, string phone,string email) : base(department, salary,date, name, phone, email)
        {
            this.officeHours = o;
            this.rank = r;
        }

        public override string ToString()
        {
            return "\n\t Student : \n Name : " + pName + "\n Phone Number : " + phoneNo +
                "\n Email : " + email + "\n Department : " + department + "\n Salary : " + salary + " $" + "\n Date Hired : " + dateHired
                +"\n Office Hours :"+officeHours + "\n Rank :"+rank;
        }

        public override double CalculateBonus()
        {
            return 1000 + salary * 0.05;
        }
        public override double CalculateVacation()
        {
            int year = Convert.ToInt32(DateTime.Now.Year - Convert.ToInt32(dateHired));
            if (year >= 3)
            {             
                if(rank=="Senior Lecturer")
                {
                    return 6;
                }
                else
                {
                    return 5;
                }
            }
            else
            {
                return 4;
            }
        }

    }
}
