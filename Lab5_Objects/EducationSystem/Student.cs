﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EducationSystem
{
   public class Student : Person 
    {
        private string major;
        public Student(string m)
        {
            this.major = m;
        }
        public Student(string m, string n, string p,string e) : base(n, p, e)
        {
            this.major = m;
        }
        public override string ToString()
        {
            return "\n\t Student : \n Name : " + pName + "\n Phone Number : " + phoneNo +
                "\n Email : " + email + "\n Major : " + major;
        }
    }
}
