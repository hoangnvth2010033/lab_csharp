﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Example_9._2
{
     interface IFerari
    {
        string Model { get; }
        string Driver { get; }
        string UserBrakers();
        string PushGasPedal();
    }
}
