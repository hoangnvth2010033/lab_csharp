﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Example_9._2
{
    public class Ferrari:IFerari
    {
        string Model { get; set; } = "911-Spider";
        public string Driver { get; set; }
        public Ferrari(string driver)
        {
            Driver = driver;
        }
        public string PushGasPedal()
        {
            return "Gooo !!";
        }
        public string UserBrakes()
        {
            return "Brakes ";
        }
    }
}
