﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Example09_Interface_ObjectClass
{
    public class Citizen:IPerson,IBirthable,IIdentifiable
    {
        public string Name { get; set; }
        public int Age { get; set; }

        public string Id { get; set; }
        public string BirthDate { get; set; }
        public Citizen(string name,int age,string id,string birth)
        {
            Name = name;
            Age = age;
            Id = id;
            BirthDate = birth;
        }
    }
}
