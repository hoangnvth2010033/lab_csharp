﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab6_Objectives
{
    class Program
    {
      static void Main(string[] args)
        {
            Thermostat thermostat = new Thermostat();
            Heater heater = new Heater(60);
            thermostat.OnTeamperatureChange += heater.OnTemperatureChanged;
            Heater heaterCuong = new Heater(100);
            thermostat.OnTeamperatureChange += heaterCuong.OnTemperatureChanged;

            Cooler cooler = new Cooler(80);
            thermostat.OnTeamperatureChange += cooler.OnTemperatureChanged;
            string temperature;
            Console.Write("Enter temperature : ");
            temperature = Console.ReadLine();
            thermostat.CurrentTemperature = int.Parse(temperature);
            Console.ReadLine();
        }
    }
}
